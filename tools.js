/**
 * @author elser
 * @module tools
 * @version 0.0.1
 */

var _ = require('underscore')
,   reexec = function (str){
        var res = RegExp.prototype.exec.call(this, str);
        if(res) for(var idx in this.groups){
            res[this.groups[idx]] = res[parseInt(idx)]; 
        }
        return res;
    }
;

module.exports = tools = {
    uniqueID : function (args){
        var ts  = new Date().getTime()
        ,   res = []
        ,   hash = []
        ,   varify = args.varicase 
    		?   function(str){
    	    		var astr = str.split('');
    	    		for(var s = 0; s < astr.length; s++){
    	    			if(Math.random().toString()[6] & 1) {
    	    				astr[s] = astr[s].toUpperCase();
    	    			} else {
    	    				astr[s] = astr[s].toLowerCase();
    	    			}
    	    		}
    	    		return astr.join('');
    	    	}
	    	:   function(str) { return str.toLowerCase() }
        ;
        args.separator = args.separator || '';
        args.base = args.base || 36;
        if( args.prefix ) res.push( args.prefix );
        res.push( varify( ts.toString(args.base) ) );
        hash.push( varify( ts.toString(args.base) ) );
        if(args.rnd || args.rndbase) {
        	args.rndbase = args.rndbase || args.base;
            var rnd = varify( Math.round(Math.random()*ts).toString(args.rndbase) );
            res.push( rnd );
        	hash.push( rnd );
        }
        if(args.postfix) res.push(args.postfix);
        
        function uid(type){
            var lock = {
                args: args,
                hash: hash,
                res:  res,
                rnd:  rnd,
                ts:   ts
            };
            type = type || lock.args['return'];

            if(type === 'Object' || type === '{}'){
                lock.args.hash = lock.hash;
                lock.args.result = lock.res;
                return args;
            } else if(type === 'Array' || type === '[]') {
                return lock.res
            } else if(type === 'Function' || type === '()') {
                lock.args['return'] = "String";
                return uid;
            } else return lock.res.join(lock.args.separator || '');
        }
        return uid( args['return'] );
    },

    /*
     * // regular expressions with named groups in which res = /(ab)(varname=[a-z]+)/g.exec('abcde') store 'ab' in res[1] and 'cde' in res[2] and res.varname
     * 
     * var res, reng = new tools.RegExpNG(/(count=\d+(?:\.\d+)?)\s*(metric=[a-zа-я]+\.?)/, 'ig');
     * while( res = reng.exec('ботинки: 10 шт.; туфли: 12 шт. (по 2150.50 руб.); крем для обуви: 1.5 л.') ){
     *     console.log(res.count, res.metric);
     * }
     * // 10 шт.
     * // 12 шт.
     * // 2150.50 руб.
     * // 1.5 л.
     */
    RegExpNG : function RegExpNG ( re, flags ) {
        if(re instanceof RegExp) {
            re = /^\/(.+)\/([igm]{,3})?$/g.exec(re.toString())[1];
        }
        var res
        ,   varName
        ,   tre = re
        ,   group = 1
        ,   groups = {}
        ,   gre = /\([^\(\)]+/g
        ;
        while( res = gre.exec(re) ){
            if( !( /\(\?[\:\!\=]/.test(res[0]) ) ){
                if( varName = /\(([a-zA-Z0-9_$]+)=/g.exec(res[0]) ) groups[group.toString()] = varName[1];
                group ++;
            }
        }
        regexp = new RegExp( re.replace(/\([a-zA-Z0-9_$]+=/g, '('), flags );
        regexp.groups = groups;
        regexp.exec = reexec.bind(regexp);
        return regexp;
    },

    /*
     * result = tools.typeDepend.bind(someObject)( someObject.someVariable, {
     *     Object: function( callback ){
     *         // if someVariable is object
     *         // this === someObject if binding used, or tools instead
     *         // if(!!this.async){
     *         //    setTimeout(function(){
     *         //       callback(someResult); // for asyncronouse code
     *         //    }, 100);
     *         // } else {
     *         //   return result; // for syncronouse code
     *         // }
     *     },
     *     RegExp: function( callback ){
     *         // if someVariable is regular expression
     *     },
     *     // and other standart types: Array, Number, Function, String, Buffer, etc.
     *     
     *     SomeCoolClass: function( callback ){
     *         // if someVariable is instance of SomeCoolClass
     *         // NOTE: someVariable.constructor.name must be equal to the 'SomeCoolClass', function for Object type will be executed instead
     *     },
     *     default: function( callback ){
     *         // default handler, if type doesnt specified in the types argument
     *     }
     * }, function callback(result){
     *     // generaly result processing for asyncronouse or syncronouse code
     * })
     */
    typeDepend : function ( variable, types, callback ) {
        var args = Array.prototype.slice.apply( arguments, 2 )
        ,   self = this
        ,   cb = function(){
                if(callback) {
                    return callback.apply( self, Array.prototype.slice.apply( arguments, 0 ));
                }
            }
        ,   res
        ;
        if( !!variable && !!variable.constructor && !!types[ variable.constructor.name ] ) {
            return types[ variable.constructor.name ].call( this, cb );
        } else if(!!types['default']) {
            return types['default'].call(this, cb);
        } else return cb.call(this, null);
    }
}
